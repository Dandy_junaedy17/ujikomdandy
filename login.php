<?php
session_start();
require("koneksi.php");

if (isset($_SESSION['user'])) {
	$sess_username = $_SESSION['user']['username'];
	$check_user = mysqli_query($db, "SELECT * FROM user WHERE username = '$sess_username'");
	$data_user = mysqli_fetch_assoc($check_user);
	if (mysqli_num_rows($check_user) == 0) {
		header("Location: ".$cfg_baseurl."logout.php");
	} else if ($data_user['status'] == "Suspended") {
		header("Location: ".$cfg_baseurl."logout.php");
    }
}
    
    if (isset($_POST['login'])) {
		$post_username = $_POST['username'];
		$post_password = $_POST['password'];
		
		if (empty($post_username) || empty($post_password)) {
			$msg_type = "error";
			$msg_content = '<b>Gagal:</b> Mohon mengisi semua input.<script>swal("Ups!", "Mohon mengisi semua input.", "error");</script>';
		} else {
			$check_user = mysqli_query($db, "SELECT * FROM user WHERE username = '$post_username'");
			if (mysqli_num_rows($check_user) == 0) {
				$msg_type = "error";
				$msg_content = "<b>Gagal:</b> Username tidak ditemukan.";
			} else {
				$data_user = mysqli_fetch_assoc($check_user);
				$hash_password = md5($post_password);
				if ($hash_password <> $data_user['password']) {
					$msg_type = "error";
					$msg_content = "<b>Gagal:</b> Password salah.";
				} else if ($data_user['status'] == "Suspended") {
					$msg_type = "error";
					$msg_content = '<b>Gagal:</b> Akun Suspended .<script>swal("Error!", "Akun Suspended.", "error");</script>';
					
				} else {
					$_SESSION['user'] = $data_user;
					$_SESSION['welcome'] = TRUE;
					header("Location: ".$cfg_baseurl);
				}
			}
		}
	}
	
	?>
<link href="class/font.css" rel="stylesheet" type="text/css">
<style type="text/css">
.whitefont {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFF;
}
.box {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bolder;
	color: #FFF;
	background-color: #333;
	border: medium solid #999;
}
body {
	background-image: url(gambar/Dark-rustic-wood-panel-background-646433.jpg);
}
.whitefont1 {	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFF;
}
a:link {
	color: #FFFFFF;
	font-weight: bold;
}
</style>
<p>&nbsp;</p>
<form name="form1" method="post" action="">
  <div align="center">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p><strong class="whitefont"><a href="register.php"></a></strong></p>
    <table width="585" height="372" border="22" bordercolor="#F0F0F0" bgcolor="#333333">
      <tr>
        <td background="gambar/images.jpeg"><div align="center">
          <table width="296" height="175" border="0" bgcolor="#000000">
            <tr>
              <td height="41" colspan="2"><div align="center" class="box">Login</div></td>
            </tr>
            <tr>
              <td width="106" height="38"><div align="center" class="whitefont1">User :</div></td>
              <td width="180" rowspan="2" bgcolor="#000000"><div align="center">
                  <label for="username"></label>
                  <input type="text" name="username" id="username" />
                </div>
                  <div align="center">
                    <label for="password"><br />
                    </label>
                    <input type="password" name="password" id="password" />
                </div></td>
            </tr>
            <tr>
              <td height="55"><div align="center" class="whitefont1">Password :</div></td>
            </tr>
            <tr>
              <td height="41" colspan="2"><div align="center" class="box">
                  <input type="submit" name="login" id="button" value="Masuk" />
              </div></td>
            </tr>
          </table>
          <p><strong class="whitefont1"><a href="register.php" class="whitefont">Register</a></strong></p>
        </div></td>
      </tr>
    </table>
    <p>&nbsp;</p>
  </div>
</form>
