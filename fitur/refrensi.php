<?php
session_start();
require("../koneksi.php");

if (isset($_SESSION['user'])) {
	$sess_username = $_SESSION['user']['username'];
	$check_user = mysqli_query($db, "SELECT * FROM user WHERE username = '$sess_username'");
	$data_user = mysqli_fetch_assoc($check_user);
	$waiter	=	$data_user['id_level'] != "2";
	$kasir	=	$data_user['id_level'] != "3";
	$owner	=	$data_user['id_level'] != "4";
	$pelanggan=	$data_user['id_level'] != "5";	
	if (mysqli_num_rows($check_user) == 0) {
		header("Location: ".$cfg_baseurl."logout.php");
	} else if ($data_user['status'] == "Suspended") {
		header("Location: ".$cfg_baseurl."logout.php");
	} else if (!$waiter || !$kasir || !$owner || !$pelanggan)  {	
		header("Location: ".$cfg_baseurl);
    
	}
	if (isset($_POST['masakan'])) {
		$post_kode = $_POST['id_masakan'];
		$post_nama = $_POST['nama_masakan'];
		$post_harga = $_POST['harga'];
		$post_status = $_POST['status_masakan'];
		
		
		$check_masakan = mysqli_query($db, "SELECT * FROM masakan WHERE nama_masakan = '$post_nama'");
			
		if (empty($post_nama) || empty($post_harga) || empty($post_status)) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Mohon mengisi semua input.";
		} else if (mysqli_num_rows($check_masakan) > 0) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Masakan telah terdaftar.";
		} else if (strlen($post_nama) > 25) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Nama Makanan Maksimal 25 karakter.";
		} else if (strlen($post_harga) > 10) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Harga Maksimal 10 karakter.";
		} else if (strlen($post_nama) < 3) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Nama Makanan Minimal 3 karakter.";
		} else if (strlen($post_harga) < 3) {
			$msg_type = "error";
			$msg_content = "<b>Gagal:</b> Harga Minimal 3 karakter.";
		} else {
				$insert_masakan = mysqli_query($db, "INSERT INTO masakan (id_masakan, nama_masakan, harga, status_masakan) VALUES ('$post_kode', '$post_nama', '$post_harga', '$post_status')");
				if ($insert_masakan == TRUE) {
					$msg_type = "success";
					$msg_content = "<b>Berhasil:</b> Masakan telah ditambahkan.";
				} else {
					$msg_type = "error";
					$msg_content = "<b>Gagal:</b> System Error.";
				}
			}
		}
		
		if (isset($_POST['edit'])) {
		$post_id_masakan = $_GET['id_masakan'];	
	    $post_namae = $_POST['nama_masakan'];
		$post_hargae = $_POST['harga'];
		$post_statuse = $_POST['status_masakan'];
	    
	    $check_order = mysqli_query($db,"SELECT * FROM masakan WHERE id_masakan = '$post_id_masakan'");
	    $data_order = mysqli_fetch_array($check_order,MYSQLI_ASSOC);
	    
	    if (mysqli_num_rows($check_order) == 0) {
	        $msg_type = "error";
	        $msg_content = "Pesana yang dimaksud tidak ditemukan.";
	    } else if ($data_user['id_level'] != "1") {
	        $msg_type = "error";
	        $msg_content = "Level anda tidak dapat mengedit pesanan ini.";
	    } else {
	        $update_order = mysqli_query($db, "UPDATE masakan SET nama_masakan = '$post_namae', harga = '$post_hargae', status_masakan = '$post_statuse' WHERE id_masakan = '$post_id_masakan'");
	        if ($update_order == TRUE) {
	            $msg_type = "success";
	            $msg_content = $msg_content = "<b>Berhasil:</b> Pesanan berhasil diedit.<br /><b>ID Masakan:</b> $post_id_masakan<br /><b>Nama Masakan:</b> $post_namae <br /><b>Harga:</b> ".number_format($post_hargae,0,',','.')."<br /><b>Status:</b> $post_statuse";
	        } else {
	            $msg_type = "error";
	            $msg_content = "Error database. (Update)";
	        }
	    }
	} else if (isset($_POST['delete'])) {
	    $post_oid = $_GET['id_masakan'];
			$checkdb_service = mysqli_query($db, "SELECT * FROM masakan WHERE id_masakan = '$post_oid'");
			if (mysqli_num_rows($checkdb_service) == 0) {
				$msg_type = "error";
				$msg_content = "<b>Gagal:</b> Pesanan tidak ditemukan.";
			} else {
				$delete_user = mysqli_query($db, "DELETE FROM masakan WHERE id_masakan = '$post_oid'");
				if ($delete_user == TRUE) {
					$msg_type = "success";
					$msg_content = "<b>Berhasil:</b> Pesanan <b>$post_oid</b> dihapus.";
			}
		}
	}
include("../lib/header.php");
?>
<link href="../class/font.css" rel="stylesheet" type="text/css" />

<td width="792" colspan="2" valign="top" bgcolor="#333333"> 
        <!-- START CONTENT -->
<table width="680" height="350" border="1" align="center">
  <tr>
  <p></p>
    <td align="center">
    <form name="form1" method="post" action="">
      <table width="338" height="175" border="0" bgcolor="#CCCCCC">
        <tr>
          <td width="149">MASUKAN KODE</td>
          <td width="173" align="center"><label for="id_masakan"></label>
            <input type="text" name="id_masakan" id="id_masakan"></td>
        </tr>
        <tr>
          <td>NAMA MASAKAN</td>
          <td align="center"><label for="nama_masakan"></label>
            <input type="text" name="nama_masakan" id="nama_masakan"></td>
        </tr>
        <tr>
          <td>HARGA</td>
          <td align="center"><label for="harga"></label>
            <input type="text" name="harga" id="harga"></td>
        </tr>
        <tr>
          <td>STATUS</td>
          <td align="center"><label for="status_masakan"></label>
            <select name="status_masakan" id="status_masakan">
              <option value="Tersedia">TERSEDIA</option>
              <option value="Tidak Tersedia">TIDAK TERSEDIA</option>
            </select></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><input type="reset" name="reset" id="reset" value="Reset"> <input type="submit" name="masakan" id="button" value="Button"></td>
          </tr>
      </table>
   
      <p class="whitefont"><? echo $msg_content;?></p>
    </form></td>
  </tr>
</table>
<?php
	include("../lib/footer.php");
} else {
	header("Location: ".$cfg_baseurl);
}
?>