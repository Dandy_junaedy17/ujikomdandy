<?php
session_start();
require("../koneksi.php");

if (isset($_SESSION['user'])) {
    $sess_username = $_SESSION['user']['username'];
    $check_user = mysqli_query($db, "SELECT * FROM user WHERE username = '$sess_username'");
    $data_user = mysqli_fetch_assoc($check_user);
    $kasir  =   $data_user['id_level'] != "3";
    $owner  =   $data_user['id_level'] != "4";
    if (mysqli_num_rows($check_user) == 0) {
        header("Location: ".$cfg_baseurl."logout.php");
    } else if ($data_user['status'] == "Suspended") {
        header("Location: ".$cfg_baseurl."logout.php");
    } else if (!$kasir || !$owner)  {   
        header("Location: ".$cfg_baseurl);
    
    }
    if (isset($_POST['order'])) {
        $id_order           = "R-".rand(100,999);
        $post_no_meja       = $_POST['no_meja'];
        $post_tanggal       = $_POST['tanggal'];
        $pemesan            = $data_user['id_user'];
        $post_keterangan    = $_POST['keterangan'];
        
        $check_order = mysqli_query($db, "SELECT * FROM orders WHERE no_meja = '$post_no_meja'");
            
        if (empty($post_no_meja) || empty($post_tanggal) || empty($pemesan)) {
            $msg_type = "error";
            $msg_content = "<script>window.alert('Gagal : Mohon mengisi semua input');</script>";
        } else if (strlen($post_keterangan) > 165) {
            $msg_type = "error";
            $msg_content = "<script>window.alert('Gagal : Keterangan Minimal 165 karakter');</script>";
        } else {
                $insert_masakan = mysqli_query($db, "INSERT INTO  `kasir`.`orders` (`id_order` ,`no_meja` ,`tanggal` ,`id_user` ,`keterangan` ,`status_order`)VALUES ('$id_order' ,  '$post_no_meja',  '$post_tanggal',  '$pemesan',  '$post_keterangan',  'Pending');");
                $update_meja    = mysqli_query($db, "UPDATE meja SET status = 'Sedang dipakai' WHERE no_meja = '$post_no_meja'");
                if ($insert_masakan == TRUE) {

                    $msg_type = "success";
                    $msg_content = "<script>alert('ID ORDER Berhasil dipesan, Silahkan lanjutkan');window.location='buat_pesanan.php?id_order=$id_order'</script>";
                } else {
                    $msg_type = "error";
                    $msg_content = "<script>window.alert('Gagal : System Error')</script>";
                }
            }
        } if (isset($_POST['dibatalkan'])) {
        $post_oid = $_GET['id_order'];
        
            $checkdb_service = mysqli_query($db, "SELECT * FROM orders WHERE id_order = '$post_oid'");
            $data_service = mysqli_fetch_assoc($checkdb_service);
            $no_meja    =   $data_service['no_meja'];
            
            if (mysqli_num_rows($checkdb_service) == 0) {
                $msg_type = "error";
                $msg_content = "<script>window.alert('Gagal : Order tidak ditemukan')</script>";
            } else {
                $update_detail_order = mysqli_query($db, "UPDATE orders SET status_order = 'Dibatalkan' WHERE id_order = '$post_oid'");
                $update_mejab   = mysqli_query($db, "UPDATE meja SET status = 'Tersedia' WHERE no_meja = '$no_meja'");
                if ($update_detail_order == TRUE || $update_mejab == TRUE) {
                    $msg_type = "success";
                    $msg_content = "<script>window.alert('Berhasil : Order Dibatalkan')</script>";
            }
        }
        }

  include("../lib/header.php");

?>
<style type="text/css">
<!--
.style2 {color: #000000}
-->
</style>

<td width="792" colspan="2" valign="top" bgcolor="#333333"> 

<br />
<form name="form1" method="post" action="">
<? echo $msg_content;?>
      <div align="center">
        <table width="476" height="239" border="0" bgcolor="#CCCCCC" align="center">
          <tr>
            <td height="29" colspan="2"><div align="center" class="Menulink"><strong>Order Menu</strong></div></td>
          </tr>
          <tr>
            <td width="243" class="whitefont style2 style2"> No. Meja</td>
              <td width="223" align="center" bgcolor="#CCCCCC"><label for="select"></label>
                <span class="styled-select">
                <select id="no_meja" name="no_meja">
                  <option value="0">Pilih salah satu</option>
                  <?php
						$check_meja = mysqli_query($db, "SELECT * FROM meja WHERE status = 'Tersedia' ORDER BY no_meja ASC");
						while ($data_meja = mysqli_fetch_array ($check_meja)){
						?>
                  <option value="<?php echo $data_meja['no_meja']; ?>"><?php echo $data_meja['no_meja']; ?> - <?php echo $data_meja['note']; ?></option>
                  <? } ?>
                </select>
              </span></td>
          </tr>
          <tr>
            <td class="whitefont style2">Tanggal</td>
              <td align="center" bgcolor="#CCCCCC"><label for="">
              <input name="tanggal" type="date" value="<?=$date;?>" id="date" readonly="readonly" />
              </label></td>
          </tr>
          <tr>
            <td class="whitefont style2">Keterangan (Opsional)</td>
              <td align="center" bgcolor="#CCCCCC"><label for="textfield2">
              <input type="text" name="keterangan" />
              </label></td>
          </tr>
          <tr>
            <td class="whitefont style2">Nama Pemesan</td>
              <td align="center" bgcolor="#CCCCCC"><label for="textfield3">
              <input type="text" value="<?=$data_user['nama_user'];?>" readonly="readonly" />
              </label></td>
          </tr>
          <tr>
            <td colspan="2" align="center" bgcolor="#000000"><input type="reset" name="Reset" id="button" value="Ulangi">
              <input type="submit" name="order" id="order" value="Submit"></td>
          </tr>
        </table>
        <table width="146" border="0">
          <tr>
            <td><div align="center" class="Menu"><a href="../index.php" class="whitefont">Kembali</a> </div></td>
          </tr>
        </table>
      </div>
  </form>
   <table width="1050" border="1">
    <tr>
      <td width="150">ID Order</td>
      <td width="150">Tanggal</td>
      <td width="150">No Meja</td>
      <td width="150">Pemesan</td>
      <td width="150">Status</td>
      <td width="150">Aksi</td>
    </tr>
<?
$cari=$_POST['cari'];
$a = mysqli_query($db, "SELECT * FROM orders WHERE id_user = '$data_user[id_user]' AND id_order LIKE '%$cari%' ORDER BY tanggal ASC");
while ($b = mysqli_fetch_array ($a)){
$status = $b['status_order'];
?>					  
				<tr>
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>?id_order=<?php echo $b['id_order']; ?>" class="form-inline" role="form" method="POST">  
					<td><? if ($status=="Pending" || $status=="Proses"){ ?> <a href="<?$cfg_baseurl;?>buat_pesanan.php?id_order=<?=$b['id_order'];?>"> <b><?=$b['id_order'];?> </b></a> <? } else { ?> <b><?=$b['id_order'];?></b> <? } ?></td>
				  	<td><?=$b['tanggal'];?></td>
					<td><?=$b['no_meja'];?> - (<?=$b['keterangan'];?>)</td>
					<td><?=$data_user['nama_user'];?></td>
<? 
	$check_a = mysqli_query($db, "SELECT * FROM detail_order WHERE id_order = '$b[id_order]'");
	$data_a = mysqli_fetch_assoc($check_a);
	$check_harga = mysqli_query($db, "SELECT SUM(harga) AS total FROM detail_order WHERE id_order = '$b[id_order]'");
	$data_harga = mysqli_fetch_assoc($check_harga);
?>				  <td><? if ($data_a['status_detail_order']=="Diproses"){ ?> <font color="blue"> <b>Proses</b></font> 
				  <? } elseif($data_a['status_detail_order']=="Disiapkan") { ?> <font color="orange"><b>Disiapkan</b></font>
				  <? } elseif($data_a['status_detail_order']=="Diterima") { ?> <? if($status=="Sukses") { ?><font color="green"><b>Sukses</b></font><?} else { ?> <font color="black"><b>Diantarkan</b></font> <? }?>
				  <? } else { ?><? if($status=="Pending"){ ?> <font color="orange"><b>Pending</b></font> <? } elseif($status=="Dibatalkan"){ ?> <font color="red"><b>Dibatalkan</b></font><? } ?> <? }?>
				  </td>
				  <td><? if ($data_a['status_detail_order']=="Diproses"){ ?> Menunggu Pesanan 
				  <? } elseif($data_a['status_detail_order']=="Disiapkan") { ?> Proses Kirim
				  <? } elseif($data_a['status_detail_order']=="Diterima") { ?> <?if($status=="Sukses") {?>Telah dibayar<? } else { ?> Menunggu Pembayaran <?php echo number_format($data_harga['total'],0,',','.'); ?> <? } ?>
				  <? } else { ?> 
				  <? if($status=="Dibatalkan") {?>Selesai<? } else { ?> 
					  <p class="submit">
						<input type="submit" name="dibatalkan" id="button" value="Batalkan Pesanan">
					  </p>
				  <? }?>
				  <? }?>
				  </td>
				</tr>
			  </form>
<?
 }
?>	  
  </table>
</td>
  <?php
	include("../lib/footer.php");
} else {
	header("Location: ".$cfg_baseurl);
}
?>