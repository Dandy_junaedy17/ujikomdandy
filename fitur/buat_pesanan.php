<?php
session_start();
require("../koneksi.php");

if (isset($_SESSION['user'])) {
	$sess_username = $_SESSION['user']['username'];
	$check_user = mysqli_query($db, "SELECT * FROM user WHERE username = '$sess_username'");
	$data_user = mysqli_fetch_assoc($check_user);
	$kasir	=	$data_user['id_level'] != "3";
	$owner	=	$data_user['id_level'] != "4";
	if (mysqli_num_rows($check_user) == 0) {
		header("Location: ".$cfg_baseurl."logout.php");
	} else if ($data_user['status'] == "Suspended") {
		header("Location: ".$cfg_baseurl."logout.php");
	} else if (!$kasir || !$owner)  {	
		header("Location: ".$cfg_baseurl);
    
	}
	
	$id_orderget = @$_GET['id_order'];  //DAPATKAN ID ORDER
	
	if (isset($_POST['order'])) {
		$id_detail_order	= "ORDER-".rand(10,100);
		$id_masakan 		= $_POST['id_masakan'];
		$post_keterangan 	= $_POST['keterangan'];
		$jumlah_masakan 	= $_POST['jumlah_masakan'];
		
		$check_masakan = mysqli_query($db, "SELECT * FROM masakan WHERE id_masakan = '$id_masakan'");
		$data_masakan = mysqli_fetch_assoc($check_masakan);
		$harga = $jumlah_masakan*$data_masakan['harga'];
		
		
			
		if (empty($id_masakan) || empty($jumlah_masakan)) {
			$msg_type = "error";
			$msg_content = "<script>window.alert('Gagal : Mohon Mengisi Semua Input')</script>";
		} else if (strlen($post_no_meja) > 3) {
			$msg_type = "error";
			$msg_content = "<script>window.alert('Gagal : Nomor Meja Maksimal 3 Angka')</script>";
		} else if (strlen($pemesan) > 30) {
			$msg_type = "error";
			$msg_content = "<script>window.alert('Gagal : Nama Pemesan Maksimal 30 Karakter')</script>";
		} else if (strlen($post_keterangan) > 165) {
			$msg_type = "error";
			$msg_content = "<script>window.alert('Gagal : Keterangan Minimal 165 Karakter')</script>";
		} else {
				$insert_masakan = mysqli_query($db, "INSERT INTO  `kasir`.`detail_order` (`id_detail_order` ,`id_order` ,`id_masakan` ,`keterangan` ,`harga` ,`jumlah_masakan` ,`status_detail_order`)VALUES ('$id_detail_order' ,  '$id_orderget', '$id_masakan', '$post_keterangan', '$harga', '$jumlah_masakan', 'Diproses');");
				$update_order	= mysqli_query($db, "UPDATE orders SET status_order = 'Proses'  WHERE id_order = '$id_orderget'");
				if ($insert_masakan == TRUE || $update_order == TRUE) {
					$msg_type = "success";
					$msg_content = "<script>window.alert('Berhasil : Terimakasih anda telah melakukan pesanan. ID Pesanan anda $id_detail_order')</script>";
				} else {
					$msg_type = "error";
					$msg_content = "<script>window.alert('Gagal : System Error')</script>";
				}
			}
		} if (isset($_POST['diterima'])) {
	    $post_oid = $_GET['id_order'];
			$checkdb_service = mysqli_query($db, "SELECT * FROM detail_order WHERE id_order = '$post_oid'");
			if (mysqli_num_rows($checkdb_service) == 0) {
				$msg_type = "error";
				$msg_content = "<script>window.alert('Gagal : Pesanan Tidak ditemukan')</script>";
			} else {
				$update_detail_order = mysqli_query($db, "UPDATE detail_order SET status_detail_order = 'Diterima' WHERE id_order = '$post_oid'");
				if ($update_detail_order == TRUE) {
					$msg_type = "success";
					$msg_content = "<script>window.alert('Berhasil : Pesan diterima')</script>";
			}	
		}
	}
	include("../lib/header.php");

?>
	<td colspan="3" valign="top">
<table width="1100" height="370" border="0" bordercolor="#65353f" >
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
		
		<center>
			
<!-- BORDER NAME -->		
		<table width="1100" height="25" border="1">
		  <tbody>
		    <tr>
		      <td bgcolor="#333333"><font color="#FFFFFF">&nbsp; FITUR/ ENTRI ORDER / PESAN MASAKAN</font></td>
	        </tr>
	      </tbody>
	  </table>
<!-- BORDER NAME -->	
			
		<br /> <?=$msg_content;?> 
			
		  <form name="form" method="post" class="form" action="">
		    <table width="400" border="0">
		      <tbody>
		        <tr>
		          <td>ID ORDER</td>
		          <td><input name="id_order" type="text" id="textfield" value="<?=$id_orderget;?>" readonly="readonly"></td>
	            </tr>
		        <tr>
		          <td>PILIH MENU</td>
		          <td>
				   <div class="styled-select">
                    <select class="form-control" id="status" name="id_masakan">
				    <option value="0">Pilih salah satu</option>
					<?php
					$check_masakan = mysqli_query($db, "SELECT * FROM masakan WHERE status_masakan = 'Tersedia' ORDER BY nama_masakan ASC");
					while ($data_detail_order = mysqli_fetch_array ($check_masakan)){
					?>
				    <option value="<?php echo $data_detail_order['id_masakan']; ?>"><?php echo $data_detail_order['nama_masakan']; ?> - <?php echo $data_detail_order['harga']; ?></option>
					<? } ?>
					</select>
				   </div>
				  </td>
	            </tr>
		        <tr>
		          <td>KETERANGAN</td>
		          <td>
                  <input type="text" name="keterangan" id="keterangan"></td>
	            </tr>
				<tr>
		          <td>JUMLAH</td>
		          <td><input type="text" name="jumlah_masakan" id="textfield"></td>
	            </tr>
		        <tr>
		          <td height="50" colspan="2" align="center">
					  <p class="submit">
					  	<input type="reset" name="reset" id="reset" value="Reset">
				  		&nbsp;
				  		<input type="submit" name="order" id="button" value="Submit">
					  </p>
				  </td>
	            </tr>
	          </tbody>
	        </table>
	      </form>
<!-- BORDER NAME -->		
		<table width="1100" height="25" border="1" >
		  <tbody>
		    <tr>
		      <td bgcolor="#333333"><font color="#FFFFFF">&nbsp; LIST REFRENSI</font></td>
		      </tr>
		    </tbody>
		  </table>
<!-- BORDER NAME -->
			
			<br />
			
		    <table width="900" class="demo-table responsive">
		      <tbody>
		      <thead>
				<tr>
				  <th>ID Pesanan</th>
                  <th>Pelanggan</th>
                  <th>Masakan</th>
                  <th>Keterangan</th>
				  <th>Status</th>
				  <th>Aksi</th>
				</tr>
		      </thead>
<?
$cari=$_POST['cari'];
$check_detail_order = mysqli_query($db, "SELECT * FROM detail_order WHERE id_order = '$id_orderget'");
while ($data_detail_order = mysqli_fetch_array ($check_detail_order)){
	$masakan = $data_detail_order['id_masakan'];
	$status	 = $data_detail_order['status_detail_order'];
?>			  
				<tr>
			 	<form action="<?php echo $_SERVER['PHP_SELF']; ?>?id_order=<?php echo $data_detail_order['id_order']; ?>" class="form-inline" role="form" method="POST">  
                  <td><?=$data_detail_order['id_detail_order'];?></td>
				  <td><?=$data_user['nama_user'];?></td>
					<?php
					$check_masakanz = mysqli_query($db, "SELECT * FROM masakan WHERE id_masakan = '$masakan'");
					while ($data_masakan = mysqli_fetch_array ($check_masakanz)){
					$masakann = $data_masakan['nama_masakan'];
					?>			  
				  <td><b><?=$data_detail_order['jumlah_masakan'];?>x</b> <?=$masakann;?></td>
					<? } ?>				  
                  <td><?=$data_detail_order['keterangan'];?></td>
				  
                  <td><?=$data_detail_order['status_detail_order'];?></td>
				  <td width="10%">
					<? if($status=="Diterima") { ?> 
					  Selesai 
					  <? } else { ?>
					  <p class="submit">
					  <input type="submit" name="diterima" value="Diterima">
					  </p>
					<? } ?>
				  </td>
				</form>
				</tr>
<?
 }
?>				  
	          </tbody>
	        </table>
			
			 <br />
			
        </center>
	</td>
  </tr>
</table>
</td>			
			
<?php
	include("../lib/footer.php");
} else {
	header("Location: ".$cfg_baseurl);
}
?>