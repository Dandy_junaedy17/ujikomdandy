-- phpMyAdmin SQL Dump
-- version 2.11.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 05:33 PM
-- Server version: 5.0.67
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE IF NOT EXISTS `detail_order` (
  `id_detail_order` varchar(10) NOT NULL,
  `id_order` varchar(5) NOT NULL,
  `id_masakan` varchar(5) NOT NULL,
  `keterangan` varchar(165) NOT NULL,
  `harga` varchar(10) character set utf8 collate utf8_swedish_ci NOT NULL,
  `jumlah_masakan` varchar(5) NOT NULL,
  `status_detail_order` enum('Diproses','Disiapkan','Diantarkan','Diterima') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `keterangan`, `harga`, `jumlah_masakan`, `status_detail_order`) VALUES
('ORDER-49', 'R-292', '1', 'jangan pake saus', '25000', '1', 'Diantarkan'),
('ORDER-95', 'R-292', '1', 'pake saus bro', '50000', '2', 'Diantarkan'),
('ORDER-37', 'R-763', '1', 'sasa', '125000', '5', 'Diantarkan'),
('ORDER-14', 'R-763', '1', 'ASDAS', '25000', '1', 'Diantarkan'),
('ORDER-78', 'R-292', '1', '212', '25000', '1', 'Diantarkan'),
('ORDER-46', 'R-443', '1', '2121', '25000', '1', 'Diantarkan'),
('ORDER-61', 'R-681', '1', 'sasa', '250000', '10', 'Diantarkan'),
('ORDER-48', 'R-681', '1', '12', '25000', '1', 'Diantarkan'),
('ORDER-78', 'R-681', '1', '212', '25000', '1', 'Diantarkan'),
('ORDER-13', 'R-681', '1', '212', '100000', '4', 'Diantarkan'),
('ORDER-51', 'R-275', 'G-12', '212', '25000', '5', 'Diproses'),
('ORDER-45', 'R-650', '1', 'dadas', '25000', '1', 'Diantarkan'),
('ORDER-90', 'R-215', '1', '', '125000', '5', 'Diantarkan'),
('ORDER-11', 'R-215', 'G-12', 'dasdsa', '5000', '1', 'Diantarkan'),
('ORDER-20', 'R-811', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-73', 'R-811', 'G-12', '', '25000', '5', 'Diantarkan'),
('ORDER-97', 'R-415', 'G-12', '', '50000', '10', 'Diproses'),
('ORDER-87', 'R-483', '1', '', '25000', '1', 'Diantarkan'),
('ORDER-71', 'R-800', '1', '212', '50000', '2', 'Diproses'),
('ORDER-68', 'R-850', 'G-12', '', '50000', '10', 'Diantarkan'),
('ORDER-20', 'R-850', '1', '', '125000', '5', 'Diantarkan'),
('ORDER-61', 'R-393', '1', 'dadas', '125000', '5', 'Diantarkan'),
('ORDER-59', 'R-932', 'G-12', 'pake saus', '10000', '2', 'Diantarkan'),
('ORDER-95', 'R-356', '1', 'pake sambel', '125000', '5', 'Diantarkan'),
('ORDER-44', 'R-921', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-21', 'R-963', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-83', 'R-204', '1', '', '25000', '1', 'Diantarkan'),
('ORDER-93', 'R-864', 'G-12', 'pake sambel', '25000', '5', 'Diantarkan');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(5) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'ADMINISTRATOR'),
(2, 'WAITER'),
(3, 'KASIR'),
(4, 'OWNER'),
(5, 'PELANGGAN');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE IF NOT EXISTS `masakan` (
  `id_masakan` varchar(11) character set utf8 collate utf8_swedish_ci NOT NULL,
  `nama_masakan` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `harga` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `status_masakan` enum('Tersedia','Tidak Tersedia') character set utf8 collate utf8_swedish_ci NOT NULL,
  PRIMARY KEY  (`id_masakan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `harga`, `status_masakan`) VALUES
('1', 'Pizza Elank', '25000', 'Tersedia'),
('G-12', 'Tahu Pedas', '5000', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE IF NOT EXISTS `meja` (
  `no_meja` int(5) NOT NULL,
  `note` varchar(30) character set utf8 collate utf8_swedish_ci NOT NULL,
  `status` enum('Sedang dipakai','Tersedia') character set utf8 collate utf8_swedish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`no_meja`, `note`, `status`) VALUES
(1, 'Personal', 'Sedang dipakai'),
(2, 'Family', 'Tersedia'),
(3, 'Personal', 'Tersedia'),
(4, 'Family', 'Tersedia'),
(5, 'Personal', 'Tersedia'),
(6, 'Personal', 'Tersedia'),
(7, 'Personal', 'Tersedia'),
(8, 'Family', 'Tersedia'),
(9, 'Family', 'Tersedia'),
(10, 'Personal', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` varchar(10) NOT NULL,
  `no_meja` int(3) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(3) NOT NULL,
  `keterangan` text NOT NULL,
  `status_order` enum('Pending','Proses','Sukses','Dibatalkan') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
('R-840', 1, '2019-01-30', 20, 'baju warna ungu\r\n', 'Pending'),
('R-864', 1, '2019-01-30', 1, 'sasa', 'Sukses'),
('R-514', 1, '2019-01-30', 1, 'sa', 'Dibatalkan'),
('R-154', 1, '2019-01-30', 1, 'sa', 'Dibatalkan'),
('R-204', 1, '2019-01-30', 1, 'pake baju biru', 'Sukses'),
('R-963', 2, '2019-01-30', 1, '', 'Sukses'),
('R-921', 1, '2019-01-29', 1, '', 'Sukses'),
('R-356', 1, '2019-01-29', 1, 'pake baju pink\r\n', 'Sukses'),
('R-932', 1, '2019-01-29', 1, 'pake baju merah\r\n', 'Sukses');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` varchar(10) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_order` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`) VALUES
('TRX-5749', 1, 'R-864', '2019-01-30', 25000),
('TRX-5733', 1, 'R-204', '2019-01-30', 25000),
('TRX-7471', 1, 'R-963', '2019-01-30', 50000),
('TRX-6544', 1, 'R-921', '2019-01-29', 50000),
('TRX-8300', 1, 'R-356', '2019-01-29', 125000),
('TRX-8731', 1, 'R-932', '2019-01-29', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(5) NOT NULL auto_increment,
  `username` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `password` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `nama_user` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `id_level` int(1) NOT NULL,
  `status` enum('Active','Suspended') character set utf8 collate utf8_swedish_ci NOT NULL,
  PRIMARY KEY  (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `status`) VALUES
(14, 'maklogeming', '561e7485844c87f07b0a2aede7c13a30', 'salim segaf alqosam', 5, 'Active'),
(2, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'kasir', 5, 'Active'),
(3, 'waiter', 'f64cff138020a2060a9817272f563b3c', 'waiter', 2, 'Active'),
(4, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'owner', 4, 'Active'),
(5, 'pelanggan', '7f78f06d2d1262a0a222ca9834b15d9d', 'pelanggan', 5, 'Active'),
(11, 'lazy4gd', '3f7578031135aa01e0409bdf6aa488f1', 'benhail gabriel', 5, 'Active'),
(12, 'salim', 'ca6b147b8fbdd688d8ebcaa3b803c22a', 'salim', 1, 'Active'),
(13, 'sadsa', '7e3f40511b178afb7f9e2c1a7a9e55af', 'sadsa', 5, 'Active'),
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'salim', 1, 'Active'),
(15, 'mufti adam', '598b980cfce4195d6c4be14efa2e486d', 'mufti adam', 5, 'Active'),
(16, 'Akbar123', 'b1c3218a0cf9959a6560be5938f2024d', 'Akbar123', 5, 'Active'),
(17, 'qwerty', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'qwerty', 5, 'Active'),
(18, 'salim123', '5c792ab5a27f615dc22f6732e29b1ee6', 'salim segaf alqosam', 5, 'Active'),
(19, '123123', '4297f44b13955235245b2497399d7a93', '123123', 5, 'Active'),
(20, 'maklo', '96a2b853e4af532f57b958ea58c3d926', 'maklo', 5, 'Active'),
(21, 'mantul', '407339954babda0c4ab38ceef81c0bda', 'hilman', 5, 'Active'),
(22, 'abay20', 'aff4b352312d5569903d88e0e68d3fbb', 'abay', 5, 'Active');
