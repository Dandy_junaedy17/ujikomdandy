<?php
session_start();
error_reporting(0);
include('../koneksi.php');
require('fpdf/fpdf.php');

date_default_timezone_set('Asia/Jakarta');// change according timezone

$currentTime = date('Y-m-d');
$from=$_POST['from'];
$end=$_POST['end'];

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(0.5,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('images/php1.jpg',1,0.1,3,3);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'TRANSAKSI RestoranKu',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telepon : +62 82112235774',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Perum RSCM Blok E2 NO.1 , Sukaraja, Kab. Bogor, Indonesia, 16710',0,'L');
$pdf->SetX(4);
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(2);
$pdf->SetFont('Arial','B',18);
$pdf->Cell(29,0.7,"Laporan Transaksi RestoranKu",0,10,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(29,0.7,"Printed On ".$currentTime,0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(7,0.7,"Date : ".$from." Sampai ".$end);
$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'ID Transaksi', 1, 0, 'C');
$pdf->Cell(7, 0.8, 'Pelanggan', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'ID Order', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Tanggal', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Total Bayar', 1, 1, 'C');
$pdf->SetFont('Arial','',9);
$no=1;
$query=mysqli_query($db,"select * from transaksi WHERE (tanggal BETWEEN '$from' AND '$end')");
while($lihat=mysqli_fetch_array($query)){
$id_user = $lihat['id_user'];
$querys=mysqli_query($db,"select * from users WHERE id_user = '$id_user'");
while($lihats=mysqli_fetch_array($querys)){
$nama = $lihats['nama_user'];
	

	$pdf->Cell(2, 0.8, $no, 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['id_transaksi'], 1, 0,'C');
	$pdf->Cell(7, 0.8, $nama, 1, 0,'C');
	$pdf->Cell(4.5, 0.8, $lihat['id_order'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['tanggal'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['total_bayar'], 1, 1,'C');

	$no++;
}}
$pdf->ln(1);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(50.5,0.7,"Approve",0,10,'C');

$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(50.5,0.7,"CEO SALIMSEGAFALQOSAM",0,10,'C');

$pdf->Output("laporan_buku.pdf","I");

?>