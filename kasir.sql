-- phpMyAdmin SQL Dump
-- version 2.11.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 23, 2019 at 12:48 PM
-- Server version: 5.0.67
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE IF NOT EXISTS `detail_order` (
  `id_detail_order` varchar(10) NOT NULL,
  `id_order` varchar(5) NOT NULL,
  `id_masakan` varchar(5) NOT NULL,
  `keterangan` varchar(165) NOT NULL,
  `harga` varchar(10) character set utf8 collate utf8_swedish_ci NOT NULL,
  `jumlah_masakan` varchar(5) NOT NULL,
  `status_detail_order` enum('Diproses','Disiapkan','Diantarkan','Diterima') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `keterangan`, `harga`, `jumlah_masakan`, `status_detail_order`) VALUES
('ORDER-49', 'R-292', '1', 'jangan pake saus', '25000', '1', 'Diantarkan'),
('ORDER-95', 'R-292', '1', 'pake saus bro', '50000', '2', 'Diantarkan'),
('ORDER-37', 'R-763', '1', 'sasa', '125000', '5', 'Diantarkan'),
('ORDER-14', 'R-763', '1', 'ASDAS', '25000', '1', 'Diantarkan'),
('ORDER-78', 'R-292', '1', '212', '25000', '1', 'Diantarkan'),
('ORDER-46', 'R-443', '1', '2121', '25000', '1', 'Diantarkan'),
('ORDER-61', 'R-681', '1', 'sasa', '250000', '10', 'Diantarkan'),
('ORDER-48', 'R-681', '1', '12', '25000', '1', 'Diantarkan'),
('ORDER-78', 'R-681', '1', '212', '25000', '1', 'Diantarkan'),
('ORDER-13', 'R-681', '1', '212', '100000', '4', 'Diantarkan'),
('ORDER-51', 'R-275', 'G-12', '212', '25000', '5', 'Diproses'),
('ORDER-45', 'R-650', '1', 'dadas', '25000', '1', 'Diantarkan'),
('ORDER-90', 'R-215', '1', '', '125000', '5', 'Diantarkan'),
('ORDER-11', 'R-215', 'G-12', 'dasdsa', '5000', '1', 'Diantarkan'),
('ORDER-20', 'R-811', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-73', 'R-811', 'G-12', '', '25000', '5', 'Diantarkan'),
('ORDER-97', 'R-415', 'G-12', '', '50000', '10', 'Diproses'),
('ORDER-87', 'R-483', '1', '', '25000', '1', 'Diantarkan'),
('ORDER-71', 'R-800', '1', '212', '50000', '2', 'Diproses'),
('ORDER-68', 'R-850', 'G-12', '', '50000', '10', 'Diantarkan'),
('ORDER-20', 'R-850', '1', '', '125000', '5', 'Diantarkan'),
('ORDER-61', 'R-393', '1', 'dadas', '125000', '5', 'Diantarkan'),
('ORDER-59', 'R-932', 'G-12', 'pake saus', '10000', '2', 'Diantarkan'),
('ORDER-95', 'R-356', '1', 'pake sambel', '125000', '5', 'Diantarkan'),
('ORDER-44', 'R-921', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-21', 'R-963', '1', '', '50000', '2', 'Diantarkan'),
('ORDER-83', 'R-204', '1', '', '25000', '1', 'Diantarkan'),
('ORDER-93', 'R-864', 'G-12', 'pake sambel', '25000', '5', 'Diantarkan'),
('ORDER-30', 'R-366', '1', 'yang pedes karetnya 2', '75000', '3', 'Diterima'),
('ORDER-20', 'R-341', '1', '', '75000', '3', 'Diterima'),
('ORDER-61', 'R-434', 'G-12', 'tahunya 1 tisunya 1 pack', '5000', '1', 'Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(5) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'ADMINISTRATOR'),
(2, 'WAITER'),
(3, 'KASIR'),
(4, 'OWNER'),
(5, 'PELANGGAN');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE IF NOT EXISTS `masakan` (
  `id_masakan` varchar(11) character set utf8 collate utf8_swedish_ci NOT NULL,
  `nama_masakan` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `harga` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `status_masakan` enum('Tersedia','Tidak Tersedia') character set utf8 collate utf8_swedish_ci NOT NULL,
  PRIMARY KEY  (`id_masakan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `harga`, `status_masakan`) VALUES
('F-001', 'Roti Unyil 4pcs', '10000', 'Tersedia'),
('F-002', 'Steak Jamur Kentang ', '15000', 'Tersedia'),
('C-caf01', 'Cappuchino', '15000', 'Tersedia'),
('C-caf02', 'Moccacino', '15000', 'Tersedia'),
('C-ice003', 'Ice Blend Cappucino', '20000', 'Tersedia'),
('C-ice004', 'Ice Milk Shake', '20000', 'Tersedia'),
('F-003', 'Beef Hotplate', '30000', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE IF NOT EXISTS `meja` (
  `no_meja` int(5) NOT NULL,
  `note` varchar(30) character set utf8 collate utf8_swedish_ci NOT NULL,
  `status` enum('Sedang dipakai','Tersedia') character set utf8 collate utf8_swedish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`no_meja`, `note`, `status`) VALUES
(1, 'Personal', 'Sedang dipakai'),
(2, 'Family', 'Sedang dipakai'),
(3, 'Personal', 'Sedang dipakai'),
(4, 'Family', 'Tersedia'),
(5, 'Personal', 'Sedang dipakai'),
(6, 'Personal', 'Tersedia'),
(7, 'Personal', 'Tersedia'),
(8, 'Family', 'Tersedia'),
(9, 'Family', 'Tersedia'),
(10, 'Personal', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` varchar(10) NOT NULL,
  `no_meja` int(3) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(3) NOT NULL,
  `keterangan` text NOT NULL,
  `status_order` enum('Pending','Proses','Sukses','Dibatalkan') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
('R-840', 1, '2019-01-30', 20, 'baju warna ungu\r\n', 'Pending'),
('R-864', 1, '2019-01-30', 1, 'sasa', 'Dibatalkan'),
('R-514', 1, '2019-01-30', 1, 'sa', 'Dibatalkan'),
('R-154', 1, '2019-01-30', 1, 'sa', 'Dibatalkan'),
('R-204', 1, '2019-01-30', 1, 'pake baju biru', 'Dibatalkan'),
('R-963', 2, '2019-01-30', 1, '', 'Dibatalkan'),
('R-921', 1, '2019-01-29', 1, '', 'Dibatalkan'),
('R-356', 1, '2019-01-29', 1, 'pake baju pink\r\n', 'Dibatalkan'),
('R-932', 1, '2019-01-29', 1, 'pake baju merah\r\n', 'Dibatalkan'),
('R-366', 1, '2019-02-16', 1, '2', 'Proses'),
('R-508', 2, '2019-02-17', 1, '42', 'Pending'),
('R-341', 2, '2019-02-17', 1, '42', 'Proses'),
('R-119', 5, '2019-02-17', 144, '', 'Pending'),
('R-434', 3, '2019-02-17', 625, 'memakai kerudung abu', 'Proses');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` varchar(10) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_order` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`) VALUES
('TRX-5749', 1, 'R-864', '2019-01-30', 25000),
('TRX-5733', 1, 'R-204', '2019-01-30', 25000),
('TRX-7471', 1, 'R-963', '2019-01-30', 50000),
('TRX-6544', 1, 'R-921', '2019-01-29', 50000),
('TRX-8300', 1, 'R-356', '2019-01-29', 125000),
('TRX-8731', 1, 'R-932', '2019-01-29', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(5) NOT NULL auto_increment,
  `username` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `password` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `nama_user` varchar(255) character set utf8 collate utf8_swedish_ci NOT NULL,
  `id_level` int(1) NOT NULL,
  `status` enum('Active','Suspended') character set utf8 collate utf8_swedish_ci NOT NULL,
  PRIMARY KEY  (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=626 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `status`) VALUES
(2, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'kasir', 5, 'Active'),
(3, 'waiter', 'f64cff138020a2060a9817272f563b3c', 'waiter', 2, 'Active'),
(4, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'owner', 4, 'Active'),
(5, 'pelanggan', '7f78f06d2d1262a0a222ca9834b15d9d', 'pelanggan', 5, 'Active'),
(1, 'bagas', '5ffd9bb73b00bce4feeb77e2d12722da', 'Bagas', 1, 'Active');
